﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quiz1_Albert.Entities
{
    public class LaptopDbContextFactory : IDesignTimeDbContextFactory<LaptopDbContext>
    {
        public LaptopDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<LaptopDbContext>();
            builder.UseSqlite("Data Source=reference.db");
            return new LaptopDbContext(builder.Options);
        }
    }
}
