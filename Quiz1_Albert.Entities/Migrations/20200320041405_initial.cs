﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Quiz1_Albert.Entities.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "laptop",
                columns: table => new
                {
                    LaptopId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    LaptopName = table.Column<string>(nullable: false),
                    LaptopQty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_laptop", x => x.LaptopId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "laptop");
        }
    }
}
