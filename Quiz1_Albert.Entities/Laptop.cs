﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Quiz1_Albert.Entities
{
    public class Laptop
    {
        /// <summary>
        /// Primary key(ID) for Laptop (Auto Increment)
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LaptopId { get; set; }

        /// <summary>
        /// Property for Laptop Name
        /// </summary>
        [Required]
        public string LaptopName { get; set; }

        /// <summary>
        /// Property for Laptop Quantity
        /// </summary>
        [Required]
        public int LaptopQty { get; set; }
    }
}
