﻿using Microsoft.EntityFrameworkCore;
using Quiz1_Albert.Entities;
using Quiz1_Albert.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Albert.Services
{
    public class LaptopService
    {
        private readonly LaptopDbContext _laptopDbContext;

        /// <summary>
        /// constructor untuk mendaftarkan LaptopDbContext
        /// </summary>
        /// <param name="dbContext"></param>
        public LaptopService(LaptopDbContext dbContext)
        {
            this._laptopDbContext = dbContext;
        }

        /// <summary>
        /// Function untuk mengambil dan menampilkan semua laptop yang ada di database(Sqlite)
        /// </summary>
        /// <returns></returns>
        public async Task<List<LaptopModel>> GetAllLaptopAsync()
        {
            var laptops = await this._laptopDbContext
                .Laptops
                .Select(Q => new LaptopModel
                {
                    LaptopId = Q.LaptopId,
                    LaptopName = Q.LaptopName,
                    LaptopQty = Q.LaptopQty
                })
                .AsNoTracking()
                .ToListAsync();
            return laptops;
        }

        /// <summary>
        /// Function untuk menambahkan/insert laptop baru 
        /// </summary>
        /// <param name="laptopModel"></param>
        /// <returns></returns>
        public async Task<bool> InsertLaptopAsync(LaptopModel laptopModel)
        {
            this._laptopDbContext.Add(new Laptop
            {
                LaptopName = laptopModel.LaptopName,
                LaptopQty = laptopModel.LaptopQty
            });
            await this._laptopDbContext.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Function untuk menghapus laptop berdasarkan id laptop
        /// </summary>
        /// <param name="laptopId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteLaptopAsync(int laptopId)
        {
            var laptopModel = await this._laptopDbContext
                .Laptops
                .Where(Q => Q.LaptopId == laptopId)
                .FirstOrDefaultAsync();

            if (laptopModel == null)
            {
                return false;
            }

            this._laptopDbContext.Remove(laptopModel);
            await this._laptopDbContext.SaveChangesAsync();
            return true;
        }


        /// <summary>
        /// Function untuk mendapatkan 1 data laptop berdasarkan laptop ID 
        /// </summary>
        /// <param name="laptopId"></param>
        /// <returns></returns>
        public async Task<LaptopModel> GetSpecificLaptopAsync (int? laptopId)
        {
            var laptops = await this._laptopDbContext
                .Laptops
                .Where(Q => Q.LaptopId == laptopId)
                .Select(Q => new LaptopModel
                {
                    LaptopId = Q.LaptopId,
                    LaptopName = Q.LaptopName,
                    LaptopQty = Q.LaptopQty
                })
                .FirstOrDefaultAsync();

            return laptops;
        }


        /// <summary>
        /// Function untuk mengupdate data laptop
        /// </summary>
        /// <param name="laptopModel"></param>
        /// <returns></returns>
        public async Task<bool> UpdateLaptopAsync(LaptopModel laptopModel)
        {
            var laptops = await this._laptopDbContext
                .Laptops
                .Where(Q => Q.LaptopId == laptopModel.LaptopId)
                .FirstOrDefaultAsync();

            if(laptops == null)
            {
                return false;
            }

            laptops.LaptopName = laptopModel.LaptopName;
            laptops.LaptopQty = laptopModel.LaptopQty;

            await this._laptopDbContext.SaveChangesAsync();
            return true;
        }


        /// <summary>
        /// Function untuk filtering data berdasarkan laptop name dan pagination 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="itemPerPage"></param>
        /// <param name="filterByName"></param>
        /// <returns></returns>
        public async Task<List<LaptopModel>> GetFilterPaginationAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            var laptopQuery = this._laptopDbContext
                .Laptops
                .AsQueryable();

            if(string.IsNullOrEmpty(filterByName) == false)
            {
                laptopQuery = laptopQuery
                    .Where(Q => Q.LaptopName.Contains(filterByName));
            }

            var laptops = await laptopQuery
                .Select(Q => new LaptopModel
                {
                    LaptopId = Q.LaptopId,
                    LaptopName = Q.LaptopName,
                    LaptopQty = Q.LaptopQty
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return laptops;
        }

        /// <summary>
        /// Function untuk menunjukkan total data dari laptop
        /// </summary>
        /// <returns></returns>
        public int GetTotalData()
        {
            var totalLaptop = this._laptopDbContext
                .Laptops
                .Count();

            return totalLaptop;
        }

    }
}
