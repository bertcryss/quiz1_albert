﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quiz1_Albert.Models;
using Quiz1_Albert.Services;

namespace Quiz1_Albert.API
{
    [Route("api/v1/laptop")]
    [ApiController]
    public class LaptopApiController : ControllerBase
    {
        private readonly LaptopService _laptopServiceMan;

        public LaptopApiController(LaptopService laptopService)
        {
            this._laptopServiceMan = laptopService;
        }
        
        /// <summary>
        /// Function API untuk memanggil function getAllLaptop di service
        /// </summary>
        /// <returns></returns>
        [HttpGet("all-laptop", Name = "getAllLaptop")]
        public async Task<ActionResult<List<LaptopModel>>> GetAllLaptopAsync()
        {
            var laptops = await this._laptopServiceMan.GetAllLaptopAsync();
            return Ok(laptops);
        }

        /// <summary>
        /// Function API untuk memanggil function insertLaptop di service
        /// </summary>
        /// <param name="value"></param>
        [HttpPost("insert-laptop", Name = "insertLaptop")]
        public async Task<ActionResult<ResponseModel>> InsertLaptopAsync ([FromBody] LaptopModel value)
        {
            var isSuccess = await this._laptopServiceMan.InsertLaptopAsync(value);
            if(isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Failed insert!"
                });
            }
            return Ok(new ResponseModel
            {
                ResponseMessage = "Insert laptop successfully!"
            });
        }

        /// <summary>
        /// Function API untuk memanggil function deleteLaptop di service
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("delete-laptop", Name = "deleteLaptop")]
        public async Task<ActionResult<ResponseModel>> DeleteLaptopAsync([FromBody]LaptopModel laptopModel)
        {
            var isSuccess = await this._laptopServiceMan.DeleteLaptopAsync(laptopModel.LaptopId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Laptop ID not found"
                });
            }
            return Ok(new ResponseModel
            {
                ResponseMessage = "Delete laptop successfully!"
            });
        }

        /// <summary>
        /// Function API untuk memanggil function getSpecificLaptop di service
        /// </summary>
        [HttpGet("get-specific-laptop", Name = "getSpecificLaptop")]
        public async Task<ActionResult<LaptopModel>> GetSpecificLaptopAsync(int? laptopId)
        {
            if (laptopId.HasValue == false) 
            {
                return BadRequest(null);
            }
           
            var laptop = await this._laptopServiceMan.GetSpecificLaptopAsync(laptopId.Value);

            if (laptop == null)
            {
                return BadRequest(null);
            }
            return Ok(laptop);
        }

        /// <summary>
        /// Function API untuk memanggil function updateLaptop di service
        /// </summary>
        [HttpPut("update-laptop", Name = "updateLaptop")]
        public async Task<ActionResult<ResponseModel>> UpdateLaptopAsync([FromBody]LaptopModel laptopModel)
        {
            var isSuccess = await this._laptopServiceMan.UpdateLaptopAsync(laptopModel);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel 
                { 
                    ResponseMessage = "Laptop ID not found!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Update laptop {laptopModel.LaptopName} successfully!"
            });
        }

        [HttpGet("filter")]
        public async Task<ActionResult<List<LaptopModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await _laptopServiceMan.GetFilterPaginationAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }

        [HttpGet("total-data")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = _laptopServiceMan.GetTotalData();

            return Ok(totalData);
        }
    }
}
