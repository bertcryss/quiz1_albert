﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Albert.Models
{
    public class ResponseModel
    {
        public string ResponseMessage { set; get; }
        public string Status { set; get; }
    }
}
