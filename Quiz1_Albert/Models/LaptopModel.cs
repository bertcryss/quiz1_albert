﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz1_Albert.Models
{
    public class LaptopModel
    {
        /// <summary>
        /// property untuk ID dari laptop
        /// </summary>
        public int LaptopId { get; set; }

        /// <summary>
        /// property untuk nama laptop
        /// </summary>
        [Required, StringLength(7, MinimumLength = 3)]
        [Display(Name = "Laptop Name")]
        public string LaptopName { get; set; }

        /// <summary>
        /// property untuk quantity/jumlah laptop
        /// </summary>
        [Required]
        [Display(Name = "Laptop Qty")]
        public int LaptopQty { get; set; }
    }
}
